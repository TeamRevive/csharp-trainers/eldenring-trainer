﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EldenringTrainer.cheats
{
    public class RunesCheat : TrainerCheat
    {
        private UIntPtr runeptr = UIntPtr.Zero;
        public RunesCheat(Mem m, string name, string desc) : base(m, name, desc) { }



        public void AddRunes(int amount)
        {
            string addr = this.runeptr.ToUInt64().ToString("X");

            int current = this.Cheat.Memory.ReadInt(addr + ",0x6C");

            current += amount;

            this.Cheat.Memory.WriteMemory(addr + ",0x6C", "int", current+"");

        }

        public override async Task<bool> Enable()
        {
            this.Cheat.AOBScan("89 41 ?? 41 2B C1 85 C0")
            .AddCodeCaveBytes(new byte[]
            {
                0x48,0x89,0x0D,0x0B,0x00,
                0x00,0x00,0x89,0x41,0x6C,
                0x44,0x29,0xC8
            }).TotalByteReplacement(6);

            bool exe = this.Cheat.CreateCodeCave();

            if (exe)
            {
                UIntPtr baseaddr = this.Cheat.CodeCaveBaseAddress;
                this.runeptr = UIntPtr.Add(baseaddr, 0x12);
                return true;
            }
            return false;
        }

        public override bool Disable()
        {
            this.Cheat.UndoCodeCave();
            return true;
        }

        public override bool IsEnabled()
        {
            return this.Cheat.Active;
        }
    }
}
