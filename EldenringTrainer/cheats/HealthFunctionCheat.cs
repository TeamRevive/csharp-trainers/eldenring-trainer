﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EldenringTrainer.cheats
{
    public class HealthFunctionCheat : TrainerCheat
    {
        public HealthFunctionCheat(Mem m, string name, string desc) : base(m, name, desc) { }


        private UIntPtr HealthFlag
        {
            get;set;
        }

        private UIntPtr InstakillFlag
        {
            get;set;
        }

        public void ToggleGodmode()
        {
            int b = this.Cheat.Memory.ReadByte(HealthFlag.ToUInt64().ToString("X"));

            if (b == 0x00)
                this.Cheat.Memory.WriteBytes(HealthFlag.ToUInt64().ToString("X"), new byte[] { 0x01 });
            else
                this.Cheat.Memory.WriteBytes(HealthFlag.ToUInt64().ToString("X"), new byte[] { 0x00 });
        }

        public void ToggleInstakill()
        {
            int b = this.Cheat.Memory.ReadByte(InstakillFlag.ToUInt64().ToString("X"));

            if (b == 0x00)
                this.Cheat.Memory.WriteBytes(InstakillFlag.ToUInt64().ToString("X"), new byte[] { 0x01 });
            else
                this.Cheat.Memory.WriteBytes(InstakillFlag.ToUInt64().ToString("X"), new byte[] { 0x00 });
        }

        public override async Task<bool> Enable()
        {
            Cheat.AOBScan("89 8? ?? 01 00 00 C3 45 89 00", true)
            .AddCodeCaveBytes(new byte[]
            {
            0x49,0x83,0xFD,0x01,0x0F,
            0x85,0x15,0x00,0x00,0x00,
            0x49,0x83,0xFE,0x01,0x0F,
            0x85,0xAF,0x00,0x00,0x00,
            0x0F,0x84,0x46,0x00,0x00,
            0x00,0xE9,0xAA,0x28,0x44,
            0x00,0x49,0x83,0xFC,0x01,
            0x0F,0x85,0x9A,0x00,0x00,
            0x00,0x49,0x83,0xFE,0x01,
            0x0F,0x85,0x90,0x00,0x00,
            0x00,0x80,0x3D,0x95,0x00,
            0x00,0x00,0x01,0x0F,0x85,
            0x83,0x00,0x00,0x00,0x81,
            0xB9,0x38,0x01,0x00,0x00,
            0x01,0x00,0x00,0x00,0x0F,
            0x8E,0x73,0x00,0x00,0x00,
            0xB8,0x01,0x00,0x00,0x00,
            0x89,0x81,0x38,0x01,0x00,
            0x00,0xE9,0x69,0x28,0x44,
            0x00,0x83,0x79,0x18,0x00,
            0x0F,0x85,0x1E,0x00,0x00,
            0x00,0x80,0x3D,0x5D,0x00,
            0x00,0x00,0x01,0x0F,0x85,
            0x4C,0x00,0x00,0x00,0x8B,
            0x81,0x3C,0x01,0x00,0x00,
            0x89,0x81,0x38,0x01,0x00,
            0x00,0xE9,0x41,0x28,0x44,
            0x00,0x81,0x79,0x18,0xF3,
            0x7F,0x00,0x00,0x48,0x89,
            0x0D,0x3B,0x00,0x00,0x00,
            0x80,0x3D,0x33,0x00,0x00,
            0x00,0x01,0x0F,0x85,0x20,
            0x00,0x00,0x00,0x81,0xB9,
            0x38,0x01,0x00,0x00,0x01,
            0x00,0x00,0x00,0x0F,0x8E,
            0x10,0x00,0x00,0x00,0xB8,
            0x01,0x00,0x00,0x00,0x89,
            0x81,0x38,0x01,0x00,0x00,
            0xE9,0x06,0x28,0x44,0x00,
            0x89,0x81,0x38,0x01,0x00,
            0x00
            }).TotalByteReplacement(6);

            bool execute = this.Cheat.CreateCodeCave();
            if (execute)
            {
                UIntPtr baseptr = this.Cheat.CodeCaveBaseAddress;
                UIntPtr healthptr = UIntPtr.Add(baseptr, 0xCE);
                UIntPtr instakillptr = UIntPtr.Add(healthptr, 0x01);

                this.HealthFlag = healthptr;
                this.InstakillFlag = instakillptr;

                return true;
            }
            return false;
        }

        public override bool Disable()
        {
            this.Cheat.UndoCodeCave();
            return true;
        }

        public override bool IsEnabled()
        {
            return this.Cheat.Active;
        }
    }
}
