﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EldenringTrainer.cheats
{
    public class Cheat
    {
        #region native_imports
        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern bool VirtualFreeEx(
            IntPtr hProcess,
            UIntPtr lpAddress,
            UIntPtr dwSize,
            uint dwFreeType
        );
        #endregion

        private static ISet<Cheat> cheats = new HashSet<Cheat>();

        private Mem m;
        private string name;
        private string desc;
        private string aob;
        private bool write;
        private bool execute;
        private byte[] codecavesbytes;
        private int injectionoffset = 0x0;
        private int bytesreplace = 5;
        private byte[] restorebytes = new byte[0];
        private bool IsActive = false;

        public Cheat(Mem m, string name, string desc)
        {
            this.m = m;
            this.name = name;
            this.desc = desc;
            cheats.Add(this);
        }

        public Mem Memory
        {
            get
            {
                return this.m;
            }
        }

        public Cheat TotalByteReplacement(int amount)
        {
            this.bytesreplace = amount;
            return this;
        }

        public Cheat AddInjectionOffset(int offset)
        {
            this.injectionoffset = offset;
            return this;
        }

        public Cheat AOBScan(string aob, bool write = false, bool execute = true)
        {
            this.aob = aob;
            this.write = write;
            this.execute = execute;
            return this;
        }

        public Cheat AddCodeCaveBytes(byte[] bytes)
        {
            this.codecavesbytes = bytes;
            return this;
        }

        public bool CreateCodeCave()
        {

            if (this.m.mProc == null || this.m.mProc.Process == null || this.m.mProc.Process.HasExited)
            {
                MessageBox.Show("Mem was null or not hooked!");
                return false;
            }
            //scan first the aob.
            //  MessageBox.Show("aob started: "+ this.aob+" flags: w("+(this.write ? "+" : "-")+")" + "R(+)" + "E(" + (this.execute ? " + " : " - ") + ")");
            Task<IEnumerable<long>> aobtask = Task.Run(() => this.m.AoBScan(this.aob, this.write, this.execute));

            aobtask.Wait();
            long aob = aobtask.Result.FirstOrDefault();
            long laddress = aob;
            // MessageBox.Show(laddress+" and address: "+laddress.ToString("X8"));
            laddress = laddress + this.injectionoffset;

            //set the 64bit or 32bit based address as a string
            if (this.m.mProc.Is64Bit)
                InjectionBaseAddress = laddress.ToString("X8");
            else
                InjectionBaseAddress = laddress.ToString("X4");

            //first backup the bytes before we continue writing our codecave.
            this.restorebytes = this.m.ReadBytes(InjectionBaseAddress, this.bytesreplace);

            //build codecave.
            UIntPtr ptr = UIntPtr.Zero;
            Task<UIntPtr> ptrtask = Task.Run(() =>
            {
                return this.m.CreateCodeCave(this.InjectionBaseAddress, this.codecavesbytes, this.bytesreplace);
            });

            ptr = ptrtask.Result;
            this.CodeCaveBaseAddress = ptrtask.Result;

            if (ptr == UIntPtr.Zero)
            {
                return false;
            }
            else
            {
                this.IsActive = !this.IsActive;
                return true;
            }
        }

        public void UndoCodeCave()
        {
            //first write our bytes back to avoid crashes because of a empty jump to nomans land :)
            this.m.WriteBytes(this.InjectionBaseAddress, this.restorebytes);
            VirtualFreeEx(this.m.mProc.Handle, this.CodeCaveBaseAddress, (UIntPtr)0, 0x8000);
        }

        public UIntPtr CodeCaveBaseAddress
        {
            get; private set;
        }

        public string InjectionBaseAddress
        {
            get; private set;
        }

        public bool Active
        {
            get
            {
                return this.IsActive;
            }
        }

        public static Cheat[] Values
        {
            get {
                return cheats.ToArray();
            }
        }
    }
}
