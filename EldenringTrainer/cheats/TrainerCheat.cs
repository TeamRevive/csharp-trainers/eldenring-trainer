﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EldenringTrainer.cheats
{
    public abstract class TrainerCheat
    {
        private Mem mem;
        private Cheat cheat;

        public TrainerCheat(Mem m, string name, string desc)
        {
            this.mem = m;
            this.cheat = new Cheat(this.mem, name, desc);
        }

        public abstract Task<bool> Enable();

        public abstract bool Disable();

        public abstract bool IsEnabled();

        public Cheat Cheat
        {
            get
            {
                return this.cheat;
            }
        }

    }
}
