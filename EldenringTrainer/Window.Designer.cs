﻿namespace EldenringTrainer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.runesamount = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.instakillcheckbox = new EldenringTrainer.controls.TrainerCheckbox();
            this.instakilllabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.godmodecheckbox = new EldenringTrainer.controls.TrainerCheckbox();
            this.godmodelabel = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.healthfunccheckbox = new EldenringTrainer.controls.TrainerCheckbox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.windowroundobj = new EldenringTrainer.controls.RoundedObject();
            this.roundedObject1 = new EldenringTrainer.controls.RoundedObject();
            this.roundedObject2 = new EldenringTrainer.controls.RoundedObject();
            this.moveAbleWindow1 = new EldenringTrainer.controls.MoveAbleWindow();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.runesamount)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Location = new System.Drawing.Point(13, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(159, 31);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label2);
            this.panel5.Location = new System.Drawing.Point(6, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(148, 22);
            this.panel5.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.LightGray;
            this.label2.Location = new System.Drawing.Point(8, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Team Revive presents";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(13, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(442, 165);
            this.panel2.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoEllipsis = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.LightGray;
            this.label8.Location = new System.Drawing.Point(6, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(426, 19);
            this.label8.TabIndex = 5;
            this.label8.Text = "Game: start_protected_game.exe (v1.04) | hooked: false";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.runesamount);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Location = new System.Drawing.Point(9, 122);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(423, 22);
            this.panel7.TabIndex = 4;
            // 
            // runesamount
            // 
            this.runesamount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.runesamount.Location = new System.Drawing.Point(372, 1);
            this.runesamount.Maximum = new decimal(new int[] {
            800000,
            0,
            0,
            0});
            this.runesamount.Name = "runesamount";
            this.runesamount.Size = new System.Drawing.Size(40, 16);
            this.runesamount.TabIndex = 3;
            this.runesamount.ValueChanged += new System.EventHandler(this.runesamount_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.LightGray;
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Set Runes:";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.instakillcheckbox);
            this.panel6.Controls.Add(this.instakilllabel);
            this.panel6.Location = new System.Drawing.Point(9, 94);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(423, 22);
            this.panel6.TabIndex = 3;
            // 
            // instakillcheckbox
            // 
            this.instakillcheckbox.BackColor = System.Drawing.Color.White;
            this.instakillcheckbox.BallState = EldenringTrainer.controls.BallEnum.OFF;
            this.instakillcheckbox.Checked = false;
            this.instakillcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.instakillcheckbox.Enabled = false;
            this.instakillcheckbox.Location = new System.Drawing.Point(383, 3);
            this.instakillcheckbox.Name = "instakillcheckbox";
            this.instakillcheckbox.Size = new System.Drawing.Size(30, 15);
            this.instakillcheckbox.TabIndex = 3;
            this.instakillcheckbox.TrainerCheckState += new System.EventHandler<EldenringTrainer.controls.TrainerCheckboxArgs>(this.instakillcheckbox_TrainerCheckState);
            // 
            // instakilllabel
            // 
            this.instakilllabel.AutoSize = true;
            this.instakilllabel.BackColor = System.Drawing.Color.Transparent;
            this.instakilllabel.Enabled = false;
            this.instakilllabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instakilllabel.ForeColor = System.Drawing.Color.LightGray;
            this.instakilllabel.Location = new System.Drawing.Point(6, 3);
            this.instakilllabel.Name = "instakilllabel";
            this.instakilllabel.Size = new System.Drawing.Size(55, 13);
            this.instakilllabel.TabIndex = 1;
            this.instakilllabel.Text = "Instakill:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LightGray;
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Cheats:";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.godmodecheckbox);
            this.panel4.Controls.Add(this.godmodelabel);
            this.panel4.Location = new System.Drawing.Point(9, 66);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(423, 22);
            this.panel4.TabIndex = 2;
            // 
            // godmodecheckbox
            // 
            this.godmodecheckbox.BackColor = System.Drawing.Color.White;
            this.godmodecheckbox.BallState = EldenringTrainer.controls.BallEnum.OFF;
            this.godmodecheckbox.Checked = false;
            this.godmodecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.godmodecheckbox.Enabled = false;
            this.godmodecheckbox.Location = new System.Drawing.Point(383, 3);
            this.godmodecheckbox.Name = "godmodecheckbox";
            this.godmodecheckbox.Size = new System.Drawing.Size(30, 15);
            this.godmodecheckbox.TabIndex = 2;
            this.godmodecheckbox.TrainerCheckState += new System.EventHandler<EldenringTrainer.controls.TrainerCheckboxArgs>(this.godmodecheckbox_TrainerCheckState);
            // 
            // godmodelabel
            // 
            this.godmodelabel.AutoSize = true;
            this.godmodelabel.BackColor = System.Drawing.Color.Transparent;
            this.godmodelabel.Enabled = false;
            this.godmodelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.godmodelabel.ForeColor = System.Drawing.Color.LightGray;
            this.godmodelabel.Location = new System.Drawing.Point(6, 3);
            this.godmodelabel.Name = "godmodelabel";
            this.godmodelabel.Size = new System.Drawing.Size(64, 13);
            this.godmodelabel.TabIndex = 1;
            this.godmodelabel.Text = "Godmode:";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.healthfunccheckbox);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(9, 38);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(423, 22);
            this.panel3.TabIndex = 1;
            // 
            // healthfunccheckbox
            // 
            this.healthfunccheckbox.BackColor = System.Drawing.Color.White;
            this.healthfunccheckbox.BallState = EldenringTrainer.controls.BallEnum.OFF;
            this.healthfunccheckbox.Checked = false;
            this.healthfunccheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.healthfunccheckbox.Location = new System.Drawing.Point(383, 3);
            this.healthfunccheckbox.Name = "healthfunccheckbox";
            this.healthfunccheckbox.Size = new System.Drawing.Size(30, 15);
            this.healthfunccheckbox.TabIndex = 1;
            this.healthfunccheckbox.TrainerCheckState += new System.EventHandler<EldenringTrainer.controls.TrainerCheckboxArgs>(this.trainerCheckbox1_TrainerCheckStateAsync);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.LightGray;
            this.label3.Location = new System.Drawing.Point(6, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Health function:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(438, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "✕";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.worker_DoWorkAsync);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.worker_ProgressChanged);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.worker_RunWorkerCompleted);
            // 
            // windowroundobj
            // 
            this.windowroundobj.Border = 3;
            this.windowroundobj.BorderColor = System.Drawing.Color.Transparent;
            this.windowroundobj.Radius = 8;
            this.windowroundobj.TargetControl = this;
            // 
            // roundedObject1
            // 
            this.roundedObject1.Border = 3;
            this.roundedObject1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject1.Radius = 8;
            this.roundedObject1.TargetControl = this.panel2;
            // 
            // roundedObject2
            // 
            this.roundedObject2.Border = 3;
            this.roundedObject2.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject2.Radius = 8;
            this.roundedObject2.TargetControl = this.panel1;
            // 
            // moveAbleWindow1
            // 
            this.moveAbleWindow1.ControlTarget = null;
            this.moveAbleWindow1.MainWindowTarget = this;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(416, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "‗";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::EldenringTrainer.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(467, 243);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(483, 243);
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Elden Ring Trainer +3 for version 1.04";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.runesamount)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private controls.RoundedObject windowroundobj;
        private controls.RoundedObject roundedObject1;
        private controls.RoundedObject roundedObject2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label godmodelabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.ComponentModel.BackgroundWorker worker;
        private controls.TrainerCheckbox instakillcheckbox;
        private controls.TrainerCheckbox godmodecheckbox;
        private controls.TrainerCheckbox healthfunccheckbox;
        private System.Windows.Forms.NumericUpDown runesamount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label instakilllabel;
        private controls.MoveAbleWindow moveAbleWindow1;
        private System.Windows.Forms.Label label4;
    }
}

