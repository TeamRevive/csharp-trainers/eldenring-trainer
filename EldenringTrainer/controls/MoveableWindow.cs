﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EldenringTrainer.controls
{
    public class MoveAbleWindow : Component
    {
        private bool isMouseClicked = false;
        private Point lastLocation;
        private Form f;
        private Control[] cs = null;
        //private Cursor ungrab;
        //private Cursor grab;

        public MoveAbleWindow()
        {
            //this.ungrab = new Cursor(Properties.Resources.grabicon1.GetHicon());
            //this.grab = new Cursor(Properties.Resources.grabicon2.GetHicon());
        }

        public Form MainWindowTarget
        {
            get
            {
                return this.f;
            }
            set
            {
                this.f = value;
            }
        }

        public Control[] ControlTarget
        {
            get
            {
                return this.cs;
            }
            set
            {
                if(value == null)
                {
                    return;
                }
                this.cs = value;
                foreach(Control c in this.cs)
                {
                    this.makeMoveAble(c);
                   // c.Cursor = this.ungrab;
                }
            }
        }

        private void makeMoveAble(Control c)
        {
            c.MouseDown += new MouseEventHandler(moveableWindowHoldEvent);
            c.MouseUp += new MouseEventHandler(moveableWindowUpEvent);
            c.MouseMove += new MouseEventHandler(moveableWindowMoveEvent);

        }

        private void moveableWindowMoveEvent(object sender, MouseEventArgs e)
        {
            if (this.isMouseClicked)
            {

                this.f.Location = new Point((this.f.Location.X - lastLocation.X) + e.X, (this.f.Location.Y - lastLocation.Y) + e.Y);  

                f.Update();
            }
        }

        private void moveableWindowHoldEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                    this.isMouseClicked = true;
                    //((Control)sender).Cursor = this.grab;
                    this.lastLocation = e.Location;
            }
        }

        private void moveableWindowUpEvent(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                //((Control)sender).Cursor = this.ungrab;
                this.isMouseClicked = false;
                    
            }
        }

    }
}