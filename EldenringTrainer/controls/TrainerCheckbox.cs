﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EldenringTrainer.controls
{
    public partial class TrainerCheckbox : UserControl
    {
        private BallEnum ballstate = BallEnum.OFF;
        private bool check = false;

        public event EventHandler<TrainerCheckboxArgs> TrainerCheckState;

        public TrainerCheckbox()
        {
            InitializeComponent();
        }

        private void TrainerCheckbox_Load(object sender, EventArgs e)
        {
        }

        public bool Checked
        {
            get
            {
                return this.check;
            }
            set
            {
                this.DoEvent(value);
            }
        }

        public BallEnum BallState
        {
            get
            {
                return this.ballstate;
            }
            set
            {
                if (value == BallEnum.ON)
                {
                    this.Cursor = Cursors.Hand;
                    this.BackColor = Color.White;
                    this.ball.BackColor = Color.FromArgb(70, 0, 0, 0);
                    this.ball.Location = new Point(14, 3);
                    //base.Enabled = true;
                }
                else if (value == BallEnum.OFF)
                {
                    this.Cursor = Cursors.Hand;
                    this.BackColor = Color.White;
                    this.ball.BackColor = Color.FromArgb(70, 0, 0, 0);
                    this.ball.Location = new Point(2, 3);
                    //base.Enabled = true;
                }
                else if (value == BallEnum.DISABLED)
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.ball.Location = new Point(9, 3);
                    this.BackColor = Color.Black;
                    this.ball.BackColor = Color.DarkGray;
                    //base.Enabled = false;
                }
                this.ballstate = value;
                this.ball.Update();
                this.Update();
            }
        }

        private void checkboxclick(object sender, EventArgs e)
        {
            if (this.BallState == BallEnum.DISABLED)
                return;

            DoEvent();
        }

        public async void DoEvent()
        {
            EventHandler<TrainerCheckboxArgs> handler = TrainerCheckState;
            if (handler != null)
            {
                TrainerCheckboxArgs args = new TrainerCheckboxArgs(this.check, !this.check);

                this.BallState = BallEnum.DISABLED;

                await Task.Run(() => handler?.Invoke(this, args));

                if (args.Cancel)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        if (args.CurrentState)
                        {
                            this.check = true;
                            this.BallState = BallEnum.ON;
                        }
                        else
                        {
                            this.check = false;
                            this.BallState = BallEnum.OFF;
                        }
                    }));
                    return;
                }

                if (args.NewState)
                {
                    this.check = true;
                    this.BallState = BallEnum.ON;
                }
                else
                {
                    this.check = false;
                    this.BallState = BallEnum.OFF;
                }
            }
        }

        public async void DoEvent(bool b)
        {
            EventHandler<TrainerCheckboxArgs> handler = TrainerCheckState;
            if (handler != null)
            {
                TrainerCheckboxArgs args = new TrainerCheckboxArgs(this.check, b);

                this.BallState = BallEnum.DISABLED;

                await Task.Run(() => handler?.Invoke(this, args));

                if (args.Cancel)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        if (args.CurrentState)
                        {
                            this.check = true;
                            this.BallState = BallEnum.ON;
                        }
                        else
                        {
                            this.check = false;
                            this.BallState = BallEnum.OFF;
                        }
                    }));
                    return;
                }

                if (args.NewState)
                {
                    this.check = true;
                    this.BallState = BallEnum.ON;
                }
                else
                {
                    this.check = false;
                    this.BallState = BallEnum.OFF;
                }
            }
        }
    }

    public enum BallEnum
    {
        ON,
        OFF,
        DISABLED
    }
}
