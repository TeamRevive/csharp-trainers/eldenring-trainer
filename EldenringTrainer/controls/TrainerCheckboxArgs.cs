﻿using System;

namespace EldenringTrainer.controls
{
    public class TrainerCheckboxArgs : EventArgs
    {

        public TrainerCheckboxArgs(bool currentcheckstate, bool newstate)
        {
            this.CurrentState = currentcheckstate;
            this.NewState = newstate;
        }


        public bool CurrentState
        {
            get;private set;
        }

        public bool NewState
        {
            get;private set;
        }

        public bool Cancel
        {
            get;set;
        }

    }
}