﻿namespace EldenringTrainer.controls
{
    partial class TrainerCheckbox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ball = new System.Windows.Forms.Panel();
            this.roundedObject1 = new EldenringTrainer.controls.RoundedObject();
            this.roundedObject2 = new EldenringTrainer.controls.RoundedObject();
            this.SuspendLayout();
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ball.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ball.Location = new System.Drawing.Point(2, 3);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(10, 10);
            this.ball.TabIndex = 0;
            this.ball.Click += new System.EventHandler(this.checkboxclick);
            // 
            // roundedObject1
            // 
            this.roundedObject1.Border = 30;
            this.roundedObject1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject1.Radius = 8;
            this.roundedObject1.TargetControl = this;
            // 
            // roundedObject2
            // 
            this.roundedObject2.Border = 30;
            this.roundedObject2.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject2.Radius = 8;
            this.roundedObject2.TargetControl = this.ball;
            // 
            // TrainerCheckbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ball);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "TrainerCheckbox";
            this.Size = new System.Drawing.Size(30, 15);
            this.Load += new System.EventHandler(this.TrainerCheckbox_Load);
            this.Click += new System.EventHandler(this.checkboxclick);
            this.ResumeLayout(false);

        }

        #endregion

        private RoundedObject roundedObject1;
        private System.Windows.Forms.Panel ball;
        private RoundedObject roundedObject2;
    }
}
