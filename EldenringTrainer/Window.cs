﻿using EldenringTrainer.cheats;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EldenringTrainer
{
    public partial class Window : Form
    {
        private bool Hooked = false;
        private Mem m = new Mem();
        private SpeechSynthesizer speak;

        TrainerCheat healthfunctioncheat;
        TrainerCheat runescheat;

        public Window()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            this.moveAbleWindow1.ControlTarget = new Control[] { this };
            this.worker.RunWorkerAsync();
            this.healthfunctioncheat = new HealthFunctionCheat(this.m, "healthfunctioncheat", "the function for instakill and godmode");
            this.runescheat = new RunesCheat(this.m, "runescheat", "the function for runes");
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void worker_DoWorkAsync(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (this.m.mProc.Process == null || this.m.mProc.Process.HasExited)
                {
                    int pid = this.m.GetProcIdFromName("start_protected_game");
                    if (pid == 0)
                    {
                        int index = this.label8.Text.LastIndexOf(':');
                        this.Invoke(new MethodInvoker(delegate
                        {
                            this.label8.Text = this.label8.Text.Substring(0, index + 1) + " false";
                        }));

                        this.Hooked = false;
                    }
                    else
                    {
                        this.m.OpenProcess(pid);
                        int index = this.label8.Text.LastIndexOf(':');
                        this.Invoke(new MethodInvoker(delegate
                        {
                            this.label8.Text = this.label8.Text.Substring(0, index + 1) + " true";
                        }));
                        this.Hooked = true;
                        Console.Beep();
                        //speak.SpeakAsyncCancelAll();
                        speak.Speak("trainer activated.");
                    }
                }
                Thread.Sleep(TimeSpan.FromSeconds(10));
            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private async void trainerCheckbox1_TrainerCheckStateAsync(object sender, controls.TrainerCheckboxArgs e)
        {
            if (e.NewState)
            {

                bool b = await this.healthfunctioncheat.Enable();

                if (b)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {

                        this.godmodecheckbox.Enabled = true;
                        this.instakillcheckbox.Enabled = true;
                        this.godmodelabel.Enabled = true;
                        this.instakilllabel.Enabled = true;
                    }));
                }
                else
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.healthfunccheckbox.Cursor = Cursors.Hand;
                        this.healthfunccheckbox.Enabled = true;
                    }));
                    e.Cancel = true;
                }
            }
            else
            {
                this.healthfunctioncheat.Disable();
                this.Invoke(new MethodInvoker(delegate
                {
                    this.godmodecheckbox.Checked = false;
                    this.godmodecheckbox.Enabled = false;
                    this.instakillcheckbox.Enabled = false;
                    this.instakillcheckbox.Checked = false;
                    this.godmodelabel.Enabled = false;
                    this.instakilllabel.Enabled = false;
                }));
            }
        }

        private void godmodecheckbox_TrainerCheckState(object sender, controls.TrainerCheckboxArgs e)
        {
            if (this.healthfunctioncheat.IsEnabled())
            {
                ((HealthFunctionCheat)this.healthfunctioncheat).ToggleGodmode();
            }
        }

        private void instakillcheckbox_TrainerCheckState(object sender, controls.TrainerCheckboxArgs e)
        {
            if (this.healthfunctioncheat.IsEnabled())
            {
                ((HealthFunctionCheat)this.healthfunctioncheat).ToggleInstakill();
            }
        }

        private async void runesamount_ValueChanged(object sender, EventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                this.runesamount.Enabled = false;
            }));

            if (!this.runescheat.IsEnabled())
            {
                bool b = await this.runescheat.Enable();
                if (b)
                {
                    ((RunesCheat)this.runescheat).AddRunes((int)this.runesamount.Value);
                }
            }
            else
            {
                ((RunesCheat)this.runescheat).AddRunes((int)this.runesamount.Value);
            }

            this.Invoke(new MethodInvoker(delegate
            {
                this.runesamount.Value = 0;
                this.runesamount.Enabled = true;
            }));
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach(Cheat cheat in Cheat.Values)
            {
                cheat.UndoCodeCave();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
